# Advanced Apache Spark SQL join techniques #

Let's have following input sample data:

people.txt -> saved at HDFS under /tests/people.txt

    1,Jerry,man,USA
    2,Cathy,female,GBR
    3,Teresa,female,GBR
    4,Rut,female,USA
    5,Roasie,female,AUS
    6,Tomas,man,CZE

and peoples orders -> saved at HDFS under /tests/orders.txt

    1,pizza,2500
    1,beer,50
    1,food,3000
    2,pizza,3000
    2,drink,24000
    2,vine,40000
    3,pizza,50
    3,white-wine,4000
    3,bread,20
    4,pizza,4000
    4,food,3000
    4,vodka,30
    5,cola,4000
    5,bread,50000
    5,sangria,300

and let's train the Apache Spark SQL joins a little bit!

## 1. Take max row from the group. ##

**Task:** Get the most expensive order for every row in people.txt. Result format: name, country, price.    

**Solution:**

    case class people(id: Int, name: String, gender: String, country: String);

    case class order(id: Int, item: String, price: Long)

    val peopleDF = sc.textFile("/tests/people.txt").map(
        line=>new people(line.split(",")(0).toInt, 
                         line.split(",")(1),
                         line.split(",")(2),
                         line.split(",")(3))).toDF();

    val orderDF = sc.textFile("/tests/orders.txt").map(
            line=>new order(line.split(",")(0).toInt,
                        line.split(",")(1),
                        line.split(",")(2).toLong)
                ).toDF();
                
    peopleDF.registerTempTable("people");
    orderDF.registerTempTable("order");

    val resultDF = sqlContext.sql(" select p.name, p.country, gr.max_p FROM people p INNER JOIN "+
                              " (select o.id, max(o.price) as max_p FROM order o GROUP BY o.id) gr ON gr.id = p.id "    
    );

    resultDF.show();

Zeppelin output:

    defined class people
    defined class order
    peopleDF: org.apache.spark.sql.DataFrame = [id: int, name: string, gender: string, country: string]
    orderDF: org.apache.spark.sql.DataFrame = [id: int, item: string, price: bigint]
    resultDF: org.apache.spark.sql.DataFrame = [name: string, country: string, max_p: bigint]
    +------+-------+-----+
    |  name|country|max_p|
    +------+-------+-----+
    | Jerry|    USA| 3000|
    | Cathy|    GBR|40000|
    |Teresa|    GBR| 4000|
    |   Rut|    USA| 4000|
    |Roasie|    AUS|50000|
    +------+-------+-----+


This was pretty easy, because we didn't need any additional data for the maximal price order. Also just first row in the group
was asked. But don't worry, we will get to it...


## 2. LEFT JOIN test ##

**Task**: Get me all rows from people.txt **who didn't order** at all.    

**Solution**:

    case class people(id: Int, name: String, gendre: String, country: String);

    case class order(id: Int, item: String, price: Long)

    val peopleDF = sc.textFile("/tests/people.txt").map(
        line=>new people(line.split(",")(0).toInt, 
                         line.split(",")(1),
                         line.split(",")(2),
                         line.split(",")(3))).toDF();

    val orderDF = sc.textFile("/tests/orders.txt").map(
        line=>new order(line.split(",")(0).toInt,
                        line.split(",")(1),
                        line.split(",")(2).toLong)
                ).toDF();
                
    peopleDF.registerTempTable("people");
    orderDF.registerTempTable("order");

    val resultDF = sqlContext.sql(" select p.name, p.country from people p LEFT JOIN order o ON p.id = o.id WHERE o.id is null ");

    resultDF.show();

Zeppelin output:

    defined class people
    defined class order
    peopleDF: org.apache.spark.sql.DataFrame = [id: int, name: string, gendre: string, country: string]
    orderDF: org.apache.spark.sql.DataFrame = [id: int, item: string, price: bigint]
    resultDF: org.apache.spark.sql.DataFrame = [name: string, country: string]
    +-----+-------+
    | name|country|
    +-----+-------+
    |Tomas|    CZE|
    +-----+-------+

Classic LEFT JOIN stuff, get me all from the left table without the reference to the right side, no rocket science.

## 3 Get first N rows from the GROUP ##

Now the fun begins! 

**Task:**

Get me **first two most expensive orders** for every row in the people.txt.    


**Solution:**

    import org.apache.spark.sql.expressions.Window;
    import org.apache.spark.sql.functions._;

    case class people(id: Int, name: String, gendre: String, country: String);

    case class order(id: Int, item: String, price: Long)

    val peopleDF = sc.textFile("/tests/people.txt").map(
        line=>new people(line.split(",")(0).toInt, 
                         line.split(",")(1),
                         line.split(",")(2),
                         line.split(",")(3))).toDF();

    val orderDF = sc.textFile("/tests/orders.txt").map(
        line=>new order(line.split(",")(0).toInt,
                        line.split(",")(1),
                        line.split(",")(2).toLong)
                ).toDF();
                
    peopleDF.registerTempTable("people");
    orderDF.registerTempTable("order");

    val window = Window.partitionBy("id").orderBy(col("price").desc);

    val indexedGroupDF = orderDF.withColumn("r", row_number().over(window)).where(col("r") <= 2);

    indexedGroupDF.registerTempTable("grouped");

    val resultDF = sqlContext.sql("select p.name, r.item, r.price from people p INNER JOIN grouped r ON p.id = r.id");

    resultDF.show();
    
Zeppelin output:

    import org.apache.spark.sql.expressions.Window
    import org.apache.spark.sql.functions._
    defined class people
    defined class order
    peopleDF: org.apache.spark.sql.DataFrame = [id: int, name: string, gendre: string, country: string]
    orderDF: org.apache.spark.sql.DataFrame = [id: int, item: string, price: bigint]
    window: org.apache.spark.sql.expressions.WindowSpec = org.apache.spark.sql.expressions.WindowSpec@5d181d1e
    indexedGroupDF: org.apache.spark.sql.DataFrame = [id: int, item: string, price: bigint, r: int]
    resultDF: org.apache.spark.sql.DataFrame = [name: string, item: string, price: bigint]
    +------+----------+-----+
    |  name|      item|price|
    +------+----------+-----+
    | Jerry|      food| 3000|
    | Jerry|     pizza| 2500|
    | Cathy|      vine|40000|
    | Cathy|     drink|24000|
    |Teresa|white-wine| 4000|
    |Teresa|     pizza|   50|
    |   Rut|     pizza| 4000|
    |   Rut|      food| 3000|
    |Roasie|     bread|50000|
    |Roasie|      cola| 4000|
    +------+----------+-----+

* Window function partitioned orders table by person's id, which resulted in the groups creation. We ordered that groups by price, descending.
* By row_number() function we indexed every row in the groups.
* In the where function we restricted the dataframe just for first two rows in the groups.
* Last resultDF is just printing in the requested format.

Something is telling me that this is going to get handy during the certification exam..:)

regards

Tomas


